# Going Through The Rust Book 
## Notes on Chapter 1.
Chapter 1 is pretty easy. Mostly just installation. Packages and Dependencies. 
Rust uses TOML for it's package and dependency management. TOML is made the same man that created github and semver. Building for release with the `--release` flag seems important too.

## Notes on Chapter 2.
Making a chapter 2 directory.
Interesting how cargo uses commands like `cargo build` and `cargo run`. It's like `docker build` and `docker run`. It's not like C++ where it was `g++ main.cpp` or `clang main.cpp`. Also interesting how you can't build individual files I don't think. It seems like using cargo handles finding the main function possibly.

#### the guessing game
I skipped to the end to look at the final code. Interesting. Looks like this is going to cover random number generation, basic string usage, reading in input, the `match` statement, and `std::cmp::Ordering` basics. First impressiong is I'm wondering why do they use `Ordering::Less` and `Ordering::Greater` etc. instead of using the actual equality operator symbols like `>` and `<`.

`secret_number` is a "range" it seems like.
`loop` is an infinite loop
`println!` is a macro
`String::new()` calls the static function of String
`io::stdin()` is how reading input is done. Noticed that this example is reading in a line at a time. They is also a builder pattern going on with `expect` coming right after `read_line`. I wonder how `expect` works? 
It looks like we're setting `guess` to a match of a result when doing a `.parse()` on the `String` type. I guess that means if we have a `String` we can run a `.parse()` on it to get an integer.
The `match` statement is using a `guess.cmp(&secret_number)` as the condition. I think that means that the `.cmp()` function is returning an `enum` because of the matching happening in the `match` statement. Hmm, okay why is the  `secret_number` which is a range (I think) being used... ahh I get it. We're generating a random number from a range. That's what the `secret_number` is. Then we're using this interesting function that is kind of like an `enum` maker. If I had to guess it probably looks like this

```rust
    enum Ordering {
        Less,
        Greater,
        Equal,
    }


    impl u32 {
        fn cmp(u32 number) -> Ordering {
            if Self > number { Ordering::Greater}
            else if Self < number { Ordering::Less}
            else { Ordering::Equal}
        }
    }
```

Ok going to go back to the top and read the whole thing.
I tried writing it using pure logic from "I want a game where the use inputs a number and we output if that number matched with our number". I don't remember how to do the range thing though. The other interesting thing is how if we don't have `expect()` we get an error. That taught me that first, `expect()` works on a `Result`. Also, `expect()` expects the result to be `Ok`. 
Interesting so this is not necessarily the right way to write this code. The proper way is to write error handling code. I wonder what that would look like though. Probably a match I assume. Just a basic 
```rust
match result {
    Ok(input_message) => val
    Err(_) => continue //handle error
}```
