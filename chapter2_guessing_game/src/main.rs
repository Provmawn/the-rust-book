use std::io;
fn main() {
    loop {
        let mut guess = String::new();

        io::stdin()
        .read_line(&mut guess)
        .expect("failed to read input");

        let guess: u32 = match guess.trim().parse() {
            Ok(val) => val,
            Err(_) => continue
        };
        println!("your guess was {:?}", guess);
    }

}
